export default (expenses) => {
	return expenses
		.map((ex) => ex.amount)
		.reduce((acc, val) => acc + val, 0);
};
