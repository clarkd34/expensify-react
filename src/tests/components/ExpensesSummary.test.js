import React from 'react';
import { shallow } from 'enzyme';
import numeral from 'numeral';
import numeralIE from '../../other/numeral-ie';
import { ExpensesSummary } from '../../components/ExpensesSummary';
import expenses from '../fixtures/expenses';

numeral.register('locale', 'ie', numeralIE);

test('Should render expenses summary correctly for single expense', () => {
    const wrapper = shallow(<ExpensesSummary expenseCount={1} expenseTotal={1099} hiddenExpenses={0} />);
    expect(wrapper).toMatchSnapshot();
});

test('Should render expenses summary correctly for multiple expenses', () => {
    const wrapper = shallow(<ExpensesSummary expenseCount={23} expenseTotal={10050} hiddenExpenses={0} />);
    expect(wrapper).toMatchSnapshot();
});