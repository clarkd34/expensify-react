import React from 'react';
import { shallow } from 'enzyme';
import ExpenseListItem from '../../components/ExpenseListItem';
import expenses from '../fixtures/expenses';

test('should render ExpenseListItem with expense data', () => {
    const expense = expenses[2];
    const wrapper = shallow(<ExpenseListItem {...expense} key={expense.id} />);
    expect(wrapper).toMatchSnapshot();
});