import React from 'react';
import numeral from 'numeral';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import selectExpenses from '../selectors/expenses';
import selectExpensesTotal from '../selectors/expenses-total';

numeral.locale('ie');

export const ExpensesSummary = ({expenseCount, expenseTotal, hiddenExpenses}) => {
    const expenseWord = expenseCount === 1 ? 'expense' : 'expenses';
    const formattedTotal = numeral(expenseTotal/100).format('$0,0.00');
    const hiddenExpensesText = (hiddenExpenses > 1) ? 'expenses' : 'expense';
    const hiddenExpensesMsg = `Not showing ${hiddenExpenses} ${hiddenExpensesText} due to filters.`;
    const noHiddenExpensesMsg = `Showing all expenses. No filters active.`;
    return (
        <div className="page-header">
            <div className="content-container">
                <h1 className="page-header__title">
                    Viewing <span>{expenseCount}</span> {expenseWord} totalling <span>{formattedTotal}</span>
                </h1>
                <p>{hiddenExpenses != 0 ? hiddenExpensesMsg : noHiddenExpensesMsg}</p>
                <div className="page-header__actions">
                    <Link className="button" to="/create">Add Expense</Link>
                </div>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => {
    const visibleExpenses = selectExpenses(state.expenses, state.filters);
    const hiddenExpenses = state.expenses.length - visibleExpenses.length;
    return {
        expenseCount: visibleExpenses.length,
        expenseTotal: selectExpensesTotal(visibleExpenses),
        hiddenExpenses: hiddenExpenses
    };
}; 

export default connect(mapStateToProps)(ExpensesSummary);