import * as firebase from 'firebase';

const config = {
	apiKey: process.env.FIREBASE_API_KEY,
	authDomain: process.env.FIREBASE_AUTH_DOMAIN,
	databaseURL: process.env.FIREBASE_DATABASE_URL,
	projectId: process.env.FIREBASE_PROJECT_ID,
	storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
	messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID
};

firebase.initializeApp(config);

const database = firebase.database();
const googleAuthProvider = new firebase.auth.GoogleAuthProvider();

export { firebase, googleAuthProvider, database as default };

// database.ref('expenses').on('child_removed', (snapshot) => {
//     console.log(snapshot.key, snapshot.val());
// });

// database.ref('expenses').on('child_changed', (snapshot) => {
//     console.log(snapshot.key, snapshot.val());
// });

// database.ref('expenses').on('child_added', (snapshot) => {
//     console.log(snapshot.key, snapshot.val());
// });

// database.ref('expenses').on('value', (snapshot) => {
//     const expenses = [];
//     snapshot.forEach((childSnapshot) => {
//         expenses.push({
//             id: childSnapshot.key,
//             ...childSnapshot.val()
//         });
//     });
//     console.log(expenses);
// }, (e) => {
//     console.log('Failed to get expenses: ', e);
// });

// database.ref('expenses')
//     .once('value')
//     .then((snapshot) => {
//         const expenses = [];
//         snapshot.forEach((childSnapshot) => {
//             expenses.push({
//                 id: childSnapshot.key,
//                 ...childSnapshot.val()
//             });
//         });
//         console.log(expenses);
//     }).catch((e) => {
//         console.log('Failed to get expenses: ', e);
//     });

// const expenses = [{
//     description: 'rent',
//     note: 'January rent',
//     amount: 50000,
//     createdAt: 123342352
// }, {
//     description: 'internet',
//     note: 'Virgin Media',
//     amount: 5500,
//     createdAt: 123223532
// }, {
//     description: 'netflix',
//     note: '',
//     amount: 1400,
//     createdAt: 123342654
// }];

// database.ref('expenses').push(expenses[0]);
// database.ref('expenses').push(expenses[1]);
// database.ref('expenses').push(expenses[2]);


// database.ref('notes/-L5ERv8AQZ6X9ShMfCzv').update({
//     body: 'buy food'
// });

// database.ref('notes/-L5ERv8AQZ6X9ShMfCzv').remove();

// database.ref('notes').push({
//     title: 'Another note',
//     body: 'This is my note'
// });

// const firebaseNotes = {
//     notes: {
//         1: {
//             title: 'First note',
//             body: 'This is my note'
//         },
//         732: {
//             title: 'Another note',
//             body: 'This is my note'
//         }
//     }
// };

// const notes = [{
//     id: 12,
//     title: 'First note',
//     body: 'This is my note'
// }, {
//     id: 732,
//     title: 'Another note',
//     body: 'This is my note'
// }];

// database.ref('notes').set(notes);

// database.ref().on('value', (snapshot) => {
//     const val = snapshot.val();
//     const name = val.name;
//     const title = val.job.title;
//     const company = val.job.company;

//     console.log(`${name} is a ${title} at ${company}`)
// }, (e) => {
//     console.log('Error fetching data', e)
// });

// database.ref('location/city')
//     .once('value')
//     .then((snapshot) => {
//         const val = snapshot.val();
//         console.log(val);
//     })
//     .catch((e) => {
//         console.log('Error fetching data', e)
//     });

// database.ref().set({
//     name: 'Declan Clarke',
//     stressLevel: 6,
//     age: 28,
//     job: {
//         title: 'Software Developer',
//         company: 'Google'
//     },
//     location: {
//         city: 'Dublin',
//         country: 'Ireland'
//     }
// }).then(() => {
//     console.log('Data is saved');
// }).catch((e) => {
//     console.log('This failed', e);
// });

// database.ref().update({
//     stressLevel: 9,
//     'job/company': 'Amazon',
//     'location/city': 'Seattle'
// });

// database.ref()
//     .remove()
//     .then(() => {
//         console.log('Remove succeeded');
//     })
//     .catch((e) => {
//         console.log('Remove failed: ', e);
//     });
